﻿using Microsoft.Azure.Documents;
using System;
using System.Collections.Generic;

namespace AzureCosmosDB.Calculator.Core
{
    public class DocumentContext<T>
    {
        public string DocumentId { get; set; }

        public Uri DocumentUri { get; set; }

        public Uri CollectionUri { get; set; }

        public PartitionKey PartitionKey { get; set; }

        public T Document { get; set; }

        public double? CreateRequestChargeRU { get; set; }

        public double? ReadRequestChargeRU { get; set; }

        public double? UpdateRequestChargeRU { get; set; }

        public double? DeleteRequestChargeRU { get; set; }

        public int? DocumentSizeInBytes { get; set; }

        public List<Exception> Exceptions { get; set; } = new List<Exception>();
    }
}
