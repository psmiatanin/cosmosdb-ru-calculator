﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Core
{
    public interface IDocumentCollectionGenerator<TDoc, TOptions>
    {
        Task<IEnumerable<TDoc>> GenerateAsync(TOptions options);
    }
}
