﻿using AzureCosmosDB.Calculator.Core.Configuration;
using AzureCosmosDB.Calculator.Core.Infrastructure;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Core
{
    public interface ICRUDExecuteEngine
    {
        Task<IEnumerable<DocumentContext<TDocument>>> ExecuteAsync<TDocument>(Func<Task<IDocumentClient>> clientFactory,
                    CollectionConfiguration collectionConfig,
                    Func<Task<IEnumerable<TDocument>>> generateDocuments,
                    Func<TDocument, string> getDocumentId,
                    Func<TDocument, object> getDocumentPartitionKey,
                    CRUDOperations operations = CRUDOperations.All);
    }
    [Flags]
    public enum CRUDOperations
    {
        Create = 1,
        Read = 2,
        Update = 4,
        Delete = 8,
        All = Create | Read | Update | Delete
    }

    public class CRUDExecuteEngine : ICRUDExecuteEngine
    {
        private readonly ICollectionManager _collectionManager;
        private readonly ILogger<CRUDExecuteEngine> _logger;

        public CRUDExecuteEngine(ICollectionManager collectionManager, ILogger<CRUDExecuteEngine> logger)
        {
            _collectionManager = collectionManager;
            _logger = logger;
        }

        public async Task<IEnumerable<DocumentContext<TDocument>>> ExecuteAsync<TDocument>(Func<Task<IDocumentClient>> clientFactory, 
            CollectionConfiguration collectionConfig,
            Func<Task<IEnumerable<TDocument>>> generateDocuments,
            Func<TDocument, string> getDocumentId, 
            Func<TDocument, object> getDocumentPartitionKey,
            CRUDOperations operations = CRUDOperations.All)
        {
            // start document generation
            var generateDocumentsTask = generateDocuments();

            // create collection
            var collection = await _collectionManager.CreateAsync(await clientFactory(), collectionConfig.DatabaseId, collectionConfig);

            var collectionUri = UriFactory.CreateDocumentCollectionUri(collectionConfig.DatabaseId, collection.Id);

            var output = new List<DocumentContext<TDocument>>();

            var tasks = new List<Task>();

            foreach (var it in await generateDocumentsTask)
            {
                var documentId = getDocumentId(it);

                var context = new DocumentContext<TDocument>()
                {
                    Document = it,
                    DocumentId = documentId,
                    PartitionKey = new PartitionKey(getDocumentPartitionKey(it)),
                    DocumentUri = UriFactory.CreateDocumentUri(collectionConfig.DatabaseId, collection.Id, documentId),
                    CollectionUri = collectionUri,
                    DocumentSizeInBytes = JsonConvert.SerializeObject(it).Length
                };

                output.Add(context);

                tasks.Add(Task.Run(() => ExecuteAsync(clientFactory, context, operations)));
            }

            Task.WaitAll(tasks.ToArray());

            return output;
        }

        private async Task ExecuteAsync<TDocument>(Func<Task<IDocumentClient>> clientFactory, DocumentContext<TDocument> context, CRUDOperations operations)
        {
            var client = await clientFactory();

            //***
            if (operations.HasFlag(CRUDOperations.Create))
            {
                await CosmosDbActionAsyncAsync(client, context, CreateDocumentAsync);

                // if entity is not created all other operations does not make sense
                if (!context.CreateRequestChargeRU.HasValue)
                {
                    return;
                }
            }
            if (operations.HasFlag(CRUDOperations.Read))
            {
                await CosmosDbActionAsyncAsync(client, context, ReadDocumentAsync);
            }
            if (operations.HasFlag(CRUDOperations.Update))
            {
                await CosmosDbActionAsyncAsync(client, context, UpdateDocumentAsync);
            }
            if (operations.HasFlag(CRUDOperations.Delete))
            {
                await CosmosDbActionAsyncAsync(client, context, DeleteDocumentAsync);
            }
        }

        private async Task CreateDocumentAsync<TDocument>(IDocumentClient client, DocumentContext<TDocument> context)
        {
            try
            {
                var createResponse = await client.CreateDocumentAsync(context.CollectionUri, context.Document);
                context.CreateRequestChargeRU = createResponse.RequestCharge;
            }
            catch (Exception e)
            {
                context.Exceptions.Add(e);
            }
        }

        private async Task ReadDocumentAsync<TDocument>(IDocumentClient client, DocumentContext<TDocument> context)
        {
            try
            {
                var readResponse = await client.ReadDocumentAsync(context.DocumentUri, options: new RequestOptions() { PartitionKey = context.PartitionKey });
                context.ReadRequestChargeRU = readResponse.RequestCharge;
            }
            catch (Exception e)
            {
                context.Exceptions.Add(e);
            }
        }

        private async Task UpdateDocumentAsync<TDocument>(IDocumentClient client, DocumentContext<TDocument> context)
        {
            try
            {
                var response = await client.ReplaceDocumentAsync(context.DocumentUri, context.Document, options: new RequestOptions() { PartitionKey = context.PartitionKey });
                context.UpdateRequestChargeRU = response.RequestCharge;
            }
            catch(Exception e)
            {
                context.Exceptions.Add(e);
            }
        }

        private async Task DeleteDocumentAsync<TDocument>(IDocumentClient client, DocumentContext<TDocument> context)
        {
            try
            {
                var response = await client.DeleteDocumentAsync(context.DocumentUri, options: new RequestOptions() { PartitionKey = context.PartitionKey });
                context.DeleteRequestChargeRU = response.RequestCharge;
            }
            catch (Exception e)
            {
                context.Exceptions.Add(e);
            }
        }

        private async Task CosmosDbActionAsyncAsync<T>(IDocumentClient client, DocumentContext<T> context, 
            Func<IDocumentClient, DocumentContext<T>,  Task> action, int failedRequestAttemptCount = 10, int failedAttemptDelay = 100)
        {
            var attemptCount = 0;

            Exception exception = null;

            while (attemptCount++ < failedRequestAttemptCount)
            {
                exception = null;

                try
                {
                    await action(client, context);
                    break;
                }
                catch (DocumentClientException ex) when ((int)ex.StatusCode == 429) // retry
                {
                    exception = ex;
                    await Task.Delay(attemptCount * failedAttemptDelay);
                }
                catch (Exception ex)
                {
                    exception = ex;
                    break;
                }
            }

            if (exception != null)
            {
                context.Exceptions.Add(exception);
            }
        }

    }
}
