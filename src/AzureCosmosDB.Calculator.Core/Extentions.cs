﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Core
{
    public static class Extentions
    {
        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach (var it in items)
            {
                action(it);
            }
        }

        public static async Task<double> CalculateQueryRequestCharge<T>(this IDocumentClient client, Func<IDocumentQuery<T>> queryBuilder)
        {
            using (var ordersQuery = queryBuilder())
            {
                double requestCharge = 0;

                while (ordersQuery.HasMoreResults)
                {
                    var queryResponse = await ordersQuery.ExecuteNextAsync<T>();
                    requestCharge += queryResponse.RequestCharge;
                }

                return requestCharge;
            }
        }
    }
}
