﻿using Microsoft.Azure.Documents;
using System;

namespace AzureCosmosDB.Calculator.Core.Infrastructure
{
    public class CosmosDbCollection
    {
        public Uri DatabaseOwnerUri { get; set; }
        public Database DatabaseOwner { get; set; }
        public Uri CollectionUri { get; set; }
        public DocumentCollection Collection { get; set; }
    }
}
