﻿using Microsoft.Azure.Documents;
using System.Threading.Tasks;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Logging;
using System.Net;
using System;
using AzureCosmosDB.Calculator.Core.Configuration;

namespace AzureCosmosDB.Calculator.Core.Infrastructure
{
    public interface ICollectionManager
    {
        Task<DocumentCollection> CreateAsync(IDocumentClient client, string databaseId, CollectionConfiguration configuration);
        Task DeleteAsync(IDocumentClient client, Uri collectionUri);
    }

    public class CollectionManager : ICollectionManager
    {
        private readonly ILogger<CollectionManager> _logger;

        public CollectionManager(ILogger<CollectionManager> logger)
        {
            _logger = logger;
        }

        public virtual async Task<DocumentCollection> CreateAsync(IDocumentClient client, string databaseId, CollectionConfiguration configuration)
        {
            DocumentCollection collection = null;

            var collectionUri = UriFactory.CreateDocumentCollectionUri(databaseId, configuration.Id);

            try
            {
                var readResponse = await client.ReadDocumentCollectionAsync(collectionUri);
                collection = readResponse.Resource;
            }
            catch (DocumentClientException e) when (e.StatusCode == HttpStatusCode.NotFound)
            {
                //do nothing
            }

            if (collection != null)
            {
                if (configuration.ThrowExceptionIfExists)
                {
                    throw new Exception($"Collection with id '{configuration.Id}' exists");
                }

                if (configuration.Recreate)
                {
                    var deleteResponse = await client.DeleteDocumentCollectionAsync(collectionUri);
                    collection = null;
                }
            }

            if (collection == null)
            {
                collection = new DocumentCollection()
                {
                    Id = configuration.Id,
                    DefaultTimeToLive = configuration.DefaultTimeToLive
                };

                configuration.PartitionKeys.ForEach(item => collection.PartitionKey.Paths.Add(item));

                if (configuration.IndexingPolicy == null)
                {
                    // Leave defaults
                }
                else
                {
                    collection.IndexingPolicy.IndexingMode = configuration.IndexingPolicy.Mode ?? IndexingMode.Consistent;
                    collection.IndexingPolicy.Automatic = configuration.IndexingPolicy.Automatic ?? true;

                    collection.IndexingPolicy.IncludedPaths.Clear();
                    collection.IndexingPolicy.ExcludedPaths.Clear();

                    if (collection.IndexingPolicy.IndexingMode != IndexingMode.None)
                    {
                        configuration.IndexingPolicy.IncludedPaths.ForEach(it => collection.IndexingPolicy.IncludedPaths.Add(it));
                        configuration.IndexingPolicy.ExcludedPaths.ForEach(it => collection.IndexingPolicy.ExcludedPaths.Add(new ExcludedPath() { Path = it }));
                    }
                }

                var collectionResponse = await client.CreateDocumentCollectionIfNotExistsAsync(UriFactory.CreateDatabaseUri(databaseId), collection);
                collection = collectionResponse.Resource;
            }

            return collection;
        }

        public async Task DeleteAsync(IDocumentClient client, Uri collectionUri)
        {
            var deleteResponse = await client.DeleteDocumentCollectionAsync(collectionUri);
        }
    }
}
