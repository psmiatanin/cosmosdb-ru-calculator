﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Core.Infrastructure
{
    public static class Extentions
    {
        public static Task DeleteAsync(this ICollectionManager manager, IDocumentClient client, string databaseId, string collectionId)
        {
            var resourceUri = UriFactory.CreateDocumentCollectionUri(databaseId, collectionId);
            return manager.DeleteAsync(client, resourceUri);
        }

        public static Task DeleteAsync(this IDatabaseManager manager, IDocumentClient client, string databaseId)
        {
            var resourceUri = UriFactory.CreateDatabaseUri(databaseId);
            return manager.DeleteAsync(client, resourceUri);
        }
    }
}
