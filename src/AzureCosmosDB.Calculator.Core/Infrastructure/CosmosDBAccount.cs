﻿using Microsoft.Azure.Documents;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Core.Infrastructure
{
    public class CosmosDbAccount
    {
        public IDocumentClient Client { get; set; }

        /// <summary>
        /// In some scenarios for generating big amount of data it is required to have pool of clients.
        /// The factory allows generate the pool
        /// </summary>
        public Func<Task<IDocumentClient>> ClientFactory { get; set; }

        public List<CosmosDbDatabase> Databases { get; } = new List<CosmosDbDatabase>();
    }
}
