﻿using AzureCosmosDB.Calculator.Core.Configuration;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Core.Infrastructure
{
    public interface IDatabaseManager
    {
        Task<Database> CreateAsync(IDocumentClient client, DatabaseConfiguration options);
        Task DeleteAsync(IDocumentClient client, Uri databaseUri);
    }

    public class DatabaseManager : IDatabaseManager
    {
        private readonly ILogger<DatabaseManager> _logger;

        public DatabaseManager(ILogger<DatabaseManager> logger)
        {
            _logger = logger;
        }

        public async Task<Database> CreateAsync(IDocumentClient client, DatabaseConfiguration options)
        {
            Database database = null;

            var databaseUri = UriFactory.CreateDatabaseUri(options.Id);

            try
            {
                var readResponse = await client.ReadDatabaseAsync(databaseUri);
                database = readResponse.Resource;
            }
            catch(DocumentClientException e) when (e.StatusCode == HttpStatusCode.NotFound)
            {
                //e.StatusCode
            }

            if (database != null)
            {
                if (options.ThrowExceptionIfExists)
                {
                    throw new Exception($"Database with id '{options.Id}' exists");
                }

                if (options.Recreate)
                {
                    var deleteResponse = await client.DeleteDatabaseAsync(databaseUri);
                    database = null;
                }
            }

            if (database == null)
            {
                database = new Database()
                {
                    Id = options.Id
                };
                //
                var requestOptions = new RequestOptions()
                {
                    OfferThroughput = options.Throughput
                };

                try
                {
                    var databaseResponse = await client.CreateDatabaseIfNotExistsAsync(database, requestOptions);
                    return databaseResponse.Resource;
                }
                catch (DocumentClientException ex)
                {
                    _logger.LogError(ex, $"CosmosDB database creation error. DatabaseId={options.Id}, Account={client.ServiceEndpoint}");
                    throw;
                }
            }

            return database;
        }

        public async Task DeleteAsync(IDocumentClient client, Uri databaseUri)
        {
            var deleteResponse = await client.DeleteDatabaseAsync(databaseUri);
        }
    }
}
