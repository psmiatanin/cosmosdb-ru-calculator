﻿using AzureCosmosDB.Calculator.Core.Configuration;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Core.Infrastructure
{
    public interface IDocumentClientFactory
    {
        Task<IDocumentClient> CreateAsync(CosmosDBAccountConfiguration configuration);
    }

    public class DocumentClientFactory : IDocumentClientFactory
    {
        private readonly ILogger<DocumentClientFactory> _logger;

        public DocumentClientFactory(ILogger<DocumentClientFactory> logger)
        {
            _logger = logger;
        }

        public Task<IDocumentClient> CreateAsync(CosmosDBAccountConfiguration configuration)
        {
            var client = new DocumentClient(new Uri(configuration.Uri), configuration.AuthenticationToken);

            client.ConnectionPolicy.MaxConnectionLimit = 500;
            client.ConnectionPolicy.RetryOptions.MaxRetryAttemptsOnThrottledRequests = 20;

            return Task.FromResult<IDocumentClient>(client);
        }
    }

}
