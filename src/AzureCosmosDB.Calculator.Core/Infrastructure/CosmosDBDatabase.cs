﻿using Microsoft.Azure.Documents;
using System;
using System.Collections.Generic;

namespace AzureCosmosDB.Calculator.Core.Infrastructure
{
    public class CosmosDbDatabase
    {
        public Uri DatabaseUri { get; set; }
        public Database Database { get; set; }
        public List<CosmosDbCollection> Collections { get; } = new List<CosmosDbCollection>();
    }
}
