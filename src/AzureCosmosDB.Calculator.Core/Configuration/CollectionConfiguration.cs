﻿using Microsoft.Azure.Documents;
using System.Collections.ObjectModel;

namespace AzureCosmosDB.Calculator.Core.Configuration
{
    public class CollectionConfiguration : CosmosDBContainerConfiguration
    {
        public string DatabaseId { get; set; }
        public string[] PartitionKeys { get; set; }
        public int? DefaultTimeToLive { get; set; } = null;
        public IndexingPolicyConfiguration IndexingPolicy { get; set; } = null;
    }

    public class IndexingPolicyConfiguration
    {
        public IndexingMode? Mode { get; set; } = IndexingMode.None;
        public bool? Automatic { get; set; } = null;
        public Collection<IncludedPath> IncludedPaths { get; set; } = new Collection<IncludedPath>();
        public Collection<string> ExcludedPaths { get; set; } = new Collection<string>();
    }
}
