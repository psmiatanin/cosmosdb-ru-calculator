﻿using System.Collections.ObjectModel;

namespace AzureCosmosDB.Calculator.Core.Configuration
{
    public class CosmosDBAccountConfiguration
    {
        public string Uri { get; set; }
        public string AuthenticationToken { get; set; }
    }
}
