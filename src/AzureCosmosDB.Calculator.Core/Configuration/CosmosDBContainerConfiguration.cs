﻿
namespace AzureCosmosDB.Calculator.Core.Configuration
{
    public class CosmosDBContainerConfiguration
    {
        public string Id { get; set; }
        public int? Throughput { get; set; } = null;
        public bool ThrowExceptionIfExists { get; set; } = false;
        public bool Recreate { get; set; } = false;
    }
}
