﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AzureCosmosDB.Calculator
{
    public static class Extentions
    {
        public static T Bind<T>(this IConfigurationSection section) where T : new()
        {
            var instance = new T();
            section.Bind(instance);
            return instance;
        }
    }
}
