﻿using AutoFixture;
using AzureCosmosDB.Calculator.Boundaries.Customer.AddressBook;
using AzureCosmosDB.Calculator.Boundaries.Customer.FavouriteBasket;
using AzureCosmosDB.Calculator.Boundaries.Customer.PersonalDetails;
using AzureCosmosDB.Calculator.Boundaries.Customer.RecentOrder;
using AzureCosmosDB.Calculator.Boundaries.Customer.SavedPizzas;
using AzureCosmosDB.Calculator.Core;
using AzureCosmosDB.Calculator.Core.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace AzureCosmosDB.Calculator
{
    public class Startup
    {
        public Startup()
        { }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            // Common
            services.AddSingleton(configuration);
            services.AddLogging(c => c.AddConsole().SetMinimumLevel(LogLevel.Debug));

            // Core
            services.AddSingleton<IDocumentClientFactory, DocumentClientFactory>();
            services.AddSingleton<IDatabaseManager, DatabaseManager>();
            services.AddSingleton<ICollectionManager, CollectionManager>();
            services.AddTransient<ICRUDExecuteEngine, CRUDExecuteEngine>();

            //
            services.AddTransient<IFixture, Fixture>();

            // address book
            services.AddTransient<IAddressBookDocumentGenerator, AddressBookDocumentGenerator>();
            services.AddTransient<AddressBookCRUDExecutor, AddressBookCRUDExecutor>();
            
            // favourite baskets
            services.AddTransient<IFavouriteBasketsDocumentGenerator, FavouriteBasketsDocumentGenerator>();
            services.AddTransient<FavouriteBasketsCRUDExecutor, FavouriteBasketsCRUDExecutor>();

            // personal details
            services.AddTransient<IPersonalDetailsDocumentGenerator, PersonalDetailsDocumentGenerator>();
            services.AddTransient<PersonalDetailsCRUDExecutor, PersonalDetailsCRUDExecutor>();
            
            //
            services.AddTransient<IRecentOrdersDocumentGenerator, RecentOrdersDocumentGenerator>();
            services.AddTransient<RecentOrdersCRUDExecutor, RecentOrdersCRUDExecutor>();

            //
            services.AddTransient<ISavedPizzaDocumentGenerator, SavedPizzasDocumentGenerator>();
            services.AddTransient<SavedPizzaCRUDExecutor, SavedPizzaCRUDExecutor>();

        }

    }
}
