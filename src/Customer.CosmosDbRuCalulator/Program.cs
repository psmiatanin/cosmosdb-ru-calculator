﻿using AzureCosmosDB.Calculator.Boundaries.Customer.AddressBook;
using AzureCosmosDB.Calculator.Boundaries.Customer.FavouriteBasket;
using AzureCosmosDB.Calculator.Boundaries.Customer.PersonalDetails;
using AzureCosmosDB.Calculator.Boundaries.Customer.RecentOrder;
using AzureCosmosDB.Calculator.Boundaries.Customer.SavedPizzas;
using AzureCosmosDB.Calculator.Core;
using AzureCosmosDB.Calculator.Core.Configuration;
using AzureCosmosDB.Calculator.Core.Infrastructure;
using Microsoft.Azure.Documents;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator
{
    class Program
    {
        private static IConfiguration Configuration { get; }
        private static IServiceProvider ServiceProvider { get; }
        private static ILogger Logger { get; }

        static Program()
        {
            Configuration = new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json")
                    .Build();

            var startup = new Startup();

            var services = new ServiceCollection();
            startup.ConfigureServices(services, Configuration);

            ServiceProvider = services.BuildServiceProvider();
            Logger = ServiceProvider.GetService<ILogger<Program>>();
        }


        public static void Main(string[] args)
        {
            MainAsync(args).GetAwaiter().GetResult();

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        private static async Task MainAsync(string[] args)
        {
            var clientConfig = new CosmosDBAccountConfiguration()
            {
                Uri = Configuration["cosmosDbAccount:uri"],
                AuthenticationToken = Configuration["cosmosDbAccount:authenticationToken"],
            };

            Func<Task<IDocumentClient>> clientFactory =
                () => ServiceProvider.GetService<IDocumentClientFactory>().CreateAsync(clientConfig);

            //*** create database
            var database = await ServiceProvider.GetService<IDatabaseManager>()
                .CreateAsync(await clientFactory(), new DatabaseConfiguration()
                {
                    Id = "customer",
                    Throughput = 400,
                    Recreate = true,
                    ThrowExceptionIfExists = false
                });


            //*** customer Boundary
            var tasks = new Task[]
            {
                ServiceProvider.GetService<AddressBookCRUDExecutor>().ExecuteAsync(clientFactory, database.Id),
                ServiceProvider.GetService<PersonalDetailsCRUDExecutor>().ExecuteAsync(clientFactory, database.Id),
                ServiceProvider.GetService<FavouriteBasketsCRUDExecutor>().ExecuteAsync(clientFactory, database.Id),
                ServiceProvider.GetService<RecentOrdersCRUDExecutor>().ExecuteAsync(clientFactory, database.Id),
                ServiceProvider.GetService<SavedPizzaCRUDExecutor>().ExecuteAsync(clientFactory, database.Id),
            };

            Task.WaitAll(tasks);
        }

    }
}
