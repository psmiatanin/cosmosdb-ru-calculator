﻿using Newtonsoft.Json;
using System;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.PersonalDetails
{
    public class PersonalDetailsDocument
    {
        [JsonProperty(PropertyName = "id")]
        public Guid CustomerId { get; set; }
        public string Email { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string ContactNumber { get; set; }
        public DateTimeOffset? DateOfBirth { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public DateTimeOffset DateLastModified { get; set; }
    }
}
