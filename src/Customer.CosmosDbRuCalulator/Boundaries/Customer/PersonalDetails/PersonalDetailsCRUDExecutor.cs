﻿using AzureCosmosDB.Calculator.Core;
using AzureCosmosDB.Calculator.Core.Configuration;
using Microsoft.Azure.Documents;
using System;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.PersonalDetails
{
    public class PersonalDetailsCRUDExecutor
        : CRUDExecutor<PersonalDetailsDocument, IPersonalDetailsDocumentGenerator, PersonalDetailsDocumentGeneratorOptions>
    {
        public PersonalDetailsCRUDExecutor(ICRUDExecuteEngine engine, IPersonalDetailsDocumentGenerator generator) 
            : base(engine, generator)
        { }

        protected override string Name => "Customer Personal Details";

        public override Task ExecuteAsync(Func<Task<IDocumentClient>> clientFactory, string databaseId)
        {
            var collectionConfiguration = new CollectionConfiguration()
            {
                Id = "personal-details",
                DatabaseId = databaseId,
                Recreate = true,
                PartitionKeys = new string[] { $"/{Helpers.GetJsonPropertyName<PersonalDetailsDocument>(it => it.CustomerId)}" },
                IndexingPolicy = new IndexingPolicyConfiguration()
                {
                    Automatic = false,
                    Mode = IndexingMode.None
                }
            };

            var generatorOptions = new PersonalDetailsDocumentGeneratorOptions()
            {
                DocumentCount = 10
            };

            return ExecuteCRUDAsync
            (
                clientFactory: clientFactory,
                collectionConfiguration: collectionConfiguration,
                getDocumentId: it => it.CustomerId.ToString(),
                getPartitionKey: it => it.CustomerId.ToString(),
                generatorOptions: generatorOptions,
                operations: CRUDOperations.All
            );
        }
    }
}
