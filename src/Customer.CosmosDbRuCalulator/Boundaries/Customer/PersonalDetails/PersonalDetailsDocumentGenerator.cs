﻿using AutoFixture;
using AzureCosmosDB.Calculator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.PersonalDetails
{
    public interface IPersonalDetailsDocumentGenerator
        : IDocumentCollectionGenerator<PersonalDetailsDocument, PersonalDetailsDocumentGeneratorOptions>
    { }

    public class PersonalDetailsDocumentGenerator : IPersonalDetailsDocumentGenerator
    {
        private readonly IFixture _fixture;

        public PersonalDetailsDocumentGenerator(IFixture fixture)
        {
            _fixture = fixture ?? throw new ArgumentNullException(nameof(fixture));
        }

        public Task<IEnumerable<PersonalDetailsDocument>> GenerateAsync(PersonalDetailsDocumentGeneratorOptions options)
        {
            var ids = Helpers.GenerateUniqueGuids(options.DocumentCount);

            var items = _fixture.Build<PersonalDetailsDocument>()
                .With(it => it.CustomerId, () => { var id = ids.First(); ids.RemoveAt(0); return id; })
                .CreateMany(options.DocumentCount)
                .ToList();

            return Task.FromResult(items.AsEnumerable());
        }
    }
}
