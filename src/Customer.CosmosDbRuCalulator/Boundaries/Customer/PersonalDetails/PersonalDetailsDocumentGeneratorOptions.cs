﻿
namespace AzureCosmosDB.Calculator.Boundaries.Customer.PersonalDetails
{
    public class PersonalDetailsDocumentGeneratorOptions
    {
        public int DocumentCount { get; set; }
    }
}
