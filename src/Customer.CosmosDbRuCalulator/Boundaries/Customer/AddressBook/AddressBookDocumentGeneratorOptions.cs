﻿
namespace AzureCosmosDB.Calculator.Boundaries.Customer.AddressBook
{
    public class AddressBookDocumentGeneratorOptions
    {
        public int DocumentCount { get; set; }
        public int LocationCountPerAddressBook { get; set; }
    }
}
