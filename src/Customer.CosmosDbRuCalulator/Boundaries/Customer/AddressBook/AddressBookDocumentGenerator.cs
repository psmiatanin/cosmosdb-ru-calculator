﻿using AutoFixture;
using AzureCosmosDB.Calculator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.AddressBook
{
    public interface IAddressBookDocumentGenerator : IDocumentCollectionGenerator<AddressBookDocument, AddressBookDocumentGeneratorOptions>
    { }

    public class AddressBookDocumentGenerator : IAddressBookDocumentGenerator
    {
        private readonly IFixture _fixture;

        public AddressBookDocumentGenerator(IFixture fixture)
        {
            _fixture = fixture ?? throw new ArgumentNullException(nameof(fixture));
        }

        public Task<IEnumerable<AddressBookDocument>> GenerateAsync(AddressBookDocumentGeneratorOptions options)
        {
            var ids = Helpers.GenerateUniqueGuids(options.DocumentCount);

            var items = _fixture.Build<AddressBookDocument>()
                .With(it => it.CustomerId, () => { var id = ids.First(); ids.RemoveAt(0); return id; })
                .With(it => it.Locations, () => _fixture.CreateMany<Location>(options.LocationCountPerAddressBook).ToList())
                .CreateMany(options.DocumentCount)
                .ToList();

            return Task.FromResult(items.AsEnumerable());
        }
    }
}
