﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.AddressBook
{
    /// <summary>
    /// Root document
    /// </summary>
    public class AddressBookDocument
    {
        [JsonProperty(PropertyName = "id")]
        public Guid CustomerId { get; set; }
        public CustomerDefaultLocation DefaultLocation { get; set; }
        public List<Location> Locations { get; set; }

        public AddressBookDocument()
        {
            DefaultLocation = new CustomerDefaultLocation();
        }
    }

    public class Location
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
        public string DeliveryInstructions { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
    }

    public class CustomerDefaultLocation
    {
        public Guid LocationId { get; set; }
        public List<DateTimeOffset?> ChangeLog { get; set; }

        public CustomerDefaultLocation()
        {
            ChangeLog = new List<DateTimeOffset?>();
        }
    }

}
