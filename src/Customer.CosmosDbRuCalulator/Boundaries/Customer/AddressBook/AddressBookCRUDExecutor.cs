﻿using AzureCosmosDB.Calculator.Core;
using AzureCosmosDB.Calculator.Core.Configuration;
using Microsoft.Azure.Documents;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.AddressBook
{
    public class AddressBookCRUDExecutor : CRUDExecutor<AddressBookDocument, IAddressBookDocumentGenerator, AddressBookDocumentGeneratorOptions>
    {
        public AddressBookCRUDExecutor(ICRUDExecuteEngine engine, IAddressBookDocumentGenerator generator) 
            : base(engine, generator)
        { }

        protected override string Name => "Customer Address Book";

        public override Task ExecuteAsync(Func<Task<IDocumentClient>> clientFactory, string databaseId)
        {
            var collectionConfiguration = new CollectionConfiguration()
            {
                Id = "address-book",
                DatabaseId = databaseId,
                Recreate = true,
                PartitionKeys = new string[] { $"/{Helpers.GetJsonPropertyName<AddressBookDocument>(it => it.CustomerId)}" },
                IndexingPolicy = new IndexingPolicyConfiguration()
                {
                    Automatic = false,
                    Mode = IndexingMode.None
                }
            };

            var documentGeneratorOptions = new AddressBookDocumentGeneratorOptions()
            {
                DocumentCount = 10,
                LocationCountPerAddressBook = 5
            };

            return this.ExecuteCRUDAsync            (
                clientFactory: clientFactory,
                collectionConfiguration: collectionConfiguration,
                getDocumentId: it => it.CustomerId.ToString(),
                getPartitionKey: it => it.CustomerId.ToString(),
                generatorOptions: documentGeneratorOptions,
                operations: CRUDOperations.All
            );
        }
    }
}
