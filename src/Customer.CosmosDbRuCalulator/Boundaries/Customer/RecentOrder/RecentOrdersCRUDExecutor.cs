﻿using AzureCosmosDB.Calculator.Core;
using AzureCosmosDB.Calculator.Core.Configuration;
using Microsoft.Azure.Documents;
using System;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.RecentOrder
{
    public class RecentOrdersCRUDExecutor : CRUDExecutor<RecentOrdersDocument, IRecentOrdersDocumentGenerator, RecentOrdersDocumentGeneratorOptions>
    {
        public RecentOrdersCRUDExecutor(ICRUDExecuteEngine engine, IRecentOrdersDocumentGenerator generator)
            : base(engine, generator)
        { }

        protected override string Name => "Customer Recent Orders";

        public override Task ExecuteAsync(Func<Task<IDocumentClient>> clientFactory, string databaseId)
        {
            var collectionConfiguration = new CollectionConfiguration()
            {
                Id = "recent-orders",
                DatabaseId = databaseId,
                Recreate = true,
                PartitionKeys = new string[] { $"/{Helpers.GetJsonPropertyName<RecentOrdersDocument>(it => it.CustomerId)}" },
                IndexingPolicy = new IndexingPolicyConfiguration()
                {
                    Automatic = false,
                    Mode = IndexingMode.None
                }
            };

            var generatorOptions = new RecentOrdersDocumentGeneratorOptions()
            {
                DocumentCount = 10,
                RecentOrderCountPerCustomer = 5,
                ProductCountPerRecentOrder = 5
            };

            return ExecuteCRUDAsync
            (
                clientFactory: clientFactory,
                collectionConfiguration: collectionConfiguration,
                getDocumentId: it => it.CustomerId.ToString(),
                getPartitionKey: it => it.CustomerId.ToString(),
                generatorOptions: generatorOptions,
                operations: CRUDOperations.All
            );
        }
    }

}
