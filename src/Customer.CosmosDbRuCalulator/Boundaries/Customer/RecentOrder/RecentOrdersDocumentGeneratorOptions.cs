﻿
namespace AzureCosmosDB.Calculator.Boundaries.Customer.RecentOrder
{
    public class RecentOrdersDocumentGeneratorOptions
    {
        public int DocumentCount { get; set; }

        public int RecentOrderCountPerCustomer { get; set; }

        public int ProductCountPerRecentOrder { get; set; }
    }
}
