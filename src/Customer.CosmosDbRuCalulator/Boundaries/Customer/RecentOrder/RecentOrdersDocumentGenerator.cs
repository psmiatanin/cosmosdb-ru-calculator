﻿using AutoFixture;
using AzureCosmosDB.Calculator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.RecentOrder
{
    public interface IRecentOrdersDocumentGenerator
        : IDocumentCollectionGenerator<RecentOrdersDocument, RecentOrdersDocumentGeneratorOptions>
    { }

    public class RecentOrdersDocumentGenerator : IRecentOrdersDocumentGenerator
    {
        private readonly IFixture _fixture;

        public RecentOrdersDocumentGenerator(IFixture fixture)
        {
            _fixture = fixture ?? throw new ArgumentNullException(nameof(fixture));
        }

        public Task<IEnumerable<RecentOrdersDocument>> GenerateAsync(RecentOrdersDocumentGeneratorOptions options)
        {
            var ids = Helpers.GenerateUniqueGuids(options.DocumentCount);

            var random = new Random();

            var items = _fixture.Build<RecentOrdersDocument>()
                .With(it => it.CustomerId, () => { var id = ids.First(); ids.RemoveAt(0); return id; })
                .With(it => it.Items, () => CreateRecentOrder(random, options))
                .CreateMany(options.DocumentCount)
                .ToList();

            return Task.FromResult(items.AsEnumerable());
        }

        private List<RecentOrder> CreateRecentOrder(Random random, RecentOrdersDocumentGeneratorOptions options)
        {
            return _fixture.Build<RecentOrder>()
                .With(it => it.Products, () => CreateProducts(random, options.ProductCountPerRecentOrder))
                .CreateMany(options.RecentOrderCountPerCustomer)
                .ToList();
        }
        private List<RecentOrderProduct> CreateProducts(Random random, int productCount)
        {
            return _fixture.Build<RecentOrderProduct>()
                .Without(it => it.Items)
                .With(it => it.RemovedIngredients, () => _fixture.CreateMany<int>(random.Next(2, 10)).ToList())
                .With(it => it.SelectedIngredients, () => _fixture.CreateMany<int>(random.Next(2, 10)).ToList())
                .With(it => it.AddedIngredients, () => _fixture.CreateMany<int>(random.Next(2, 10)).ToList())
                .CreateMany(productCount)
                .ToList();
        }
    }
}
