﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.RecentOrder
{
    public class RecentOrdersDocument
    {
        [JsonProperty(PropertyName = "id")]
        public Guid CustomerId { get; set; }

        [JsonProperty(PropertyName = "ttl", NullValueHandling = NullValueHandling.Ignore)]
        public int? TimeToLive { get; set; }

        [JsonProperty(PropertyName = "items")]
        public List<RecentOrder> Items { get; set; }
    }
    public class RecentOrder
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }
        public Guid MenuId { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public List<RecentOrderProduct> Products { get; set; }
    }
    public class RecentOrderProduct
    {
        public Guid Id { get; set; }
        public int Sku { get; set; }
        public int Quantity { get; set; }
        public ProductType ProductType { get; set; }
        public List<int> SelectedIngredients { get; set; }
        public List<int> AddedIngredients { get; set; }
        public List<int> RemovedIngredients { get; set; }
        public List<RecentOrderProduct> Items { get; set; }
    }
    public enum ProductType
    {
        Pizza,
        Side,
        Dessert,
        Drink,
        Wrap
    }

}
