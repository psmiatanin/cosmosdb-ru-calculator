﻿
namespace AzureCosmosDB.Calculator.Boundaries.Customer.SavedPizzas
{
    public class SavedPizzaDocumentGeneratorOptions
    {
        public int DocumentCount { get; set; }

        public int SavedPizzaCountPerCustomer { get; set; }
    }
}
