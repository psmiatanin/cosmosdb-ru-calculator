﻿using AzureCosmosDB.Calculator.Core;
using AzureCosmosDB.Calculator.Core.Configuration;
using Microsoft.Azure.Documents;
using System;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.SavedPizzas
{
    public class SavedPizzaCRUDExecutor : CRUDExecutor<SavedPizzaDocument, ISavedPizzaDocumentGenerator, SavedPizzaDocumentGeneratorOptions>
    {
        public SavedPizzaCRUDExecutor(ICRUDExecuteEngine engine, ISavedPizzaDocumentGenerator generator)
            : base(engine, generator)
        { }

        protected override string Name => "Customer Saved Pizzas";

        public override Task ExecuteAsync(Func<Task<IDocumentClient>> clientFactory, string databaseId)
        {
            var collectionConfiguration = new CollectionConfiguration()
            {
                Id = "saved-pizzas",
                DatabaseId = databaseId,
                Recreate = true,
                PartitionKeys = new string[] { $"/{Helpers.GetJsonPropertyName<SavedPizzaDocument>(it => it.CustomerId)}" },
                IndexingPolicy = new IndexingPolicyConfiguration()
                {
                    Automatic = false,
                    Mode = IndexingMode.None
                }
            };

            var generatorOptions = new SavedPizzaDocumentGeneratorOptions()
            {
                DocumentCount = 10,
                SavedPizzaCountPerCustomer = 5
            };

            return ExecuteCRUDAsync
            (
                clientFactory: clientFactory,
                collectionConfiguration: collectionConfiguration,
                getDocumentId: it => it.Id.ToString(),
                getPartitionKey: it => it.CustomerId.ToString(),
                generatorOptions: generatorOptions,
                operations: CRUDOperations.All
            );
        }
    }

}
