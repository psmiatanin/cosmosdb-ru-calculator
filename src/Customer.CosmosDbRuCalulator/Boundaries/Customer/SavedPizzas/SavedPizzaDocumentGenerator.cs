﻿using AutoFixture;
using AzureCosmosDB.Calculator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.SavedPizzas
{
    public interface ISavedPizzaDocumentGenerator
        : IDocumentCollectionGenerator<SavedPizzaDocument, SavedPizzaDocumentGeneratorOptions>
    { }

    public class SavedPizzasDocumentGenerator : ISavedPizzaDocumentGenerator
    {
        private readonly IFixture _fixture;

        public SavedPizzasDocumentGenerator(IFixture fixture)
        {
            _fixture = fixture ?? throw new ArgumentNullException(nameof(fixture));
        }

        public Task<IEnumerable<SavedPizzaDocument>> GenerateAsync(SavedPizzaDocumentGeneratorOptions options)
        {
            var ids = Helpers.GenerateUniqueGuids(options.DocumentCount);

            var random = new Random();

            var items = _fixture.Build<SavedPizzaDocument>()
                    .With(it => it.CustomerId, () => { var id = ids.First(); ids.RemoveAt(0); return id; })
                    .With(it => it.Pizza, () => CreateSavedPizza(random))
                    .CreateMany(options.DocumentCount)
                    .ToList();

            return Task.FromResult(items.AsEnumerable());
        }
        private SavedPizzaProduct CreateSavedPizza(Random random)
        {
            return _fixture.Build<SavedPizzaProduct>()
                .Without(it => it.Items)
                .With(it => it.RemovedIngredients, () => _fixture.CreateMany<int>(random.Next(2, 10)).ToList())
                .With(it => it.SelectedIngredients, () => _fixture.CreateMany<int>(random.Next(2, 10)).ToList())
                .With(it => it.AddedIngredients, () => _fixture.CreateMany<int>(random.Next(2, 10)).ToList())
                .Create();
        }
    }
}
