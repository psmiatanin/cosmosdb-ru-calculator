﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.SavedPizzas
{
    public class SavedPizzaDocument
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public string Name { get; set; }
        public string MenuId { get; set; }
        public SavedPizzaProduct Pizza { get; set; }
        public DateTimeOffset DateCreated { get; set; }
    }
    public class SavedPizzaProduct
    {
        public Guid Id { get; set; }
        public int Sku { get; set; }
        public int Quantity { get; set; }
        public ProductType ProductType { get; set; }
        public List<int> SelectedIngredients { get; set; }
        public List<int> AddedIngredients { get; set; }
        public List<int> RemovedIngredients { get; set; }
        public List<SavedPizzaProduct> Items { get; set; }
    }
    public enum ProductType
    {
        Pizza,
        Side,
        Dessert,
        Drink,
        Wrap
    }

}
