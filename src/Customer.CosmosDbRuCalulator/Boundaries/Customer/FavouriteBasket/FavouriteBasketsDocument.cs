﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.FavouriteBasket
{
    public class FavouriteBasketsDocument
    {
        [JsonProperty(PropertyName = "id")]
        public Guid CustomerId { get; set; }
        [JsonProperty(PropertyName = "items")]
        public List<FavouriteBasket> Items { get; set; }
        public long Timestamp { get; set; }
        public string Etag { get; set; }
    }
    public class FavouriteBasket
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        public string MenuId { get; set; }
        public OrderInfo OrderInfo { get; set; }
        public DateTimeOffset DateCreated { get; set; }
        public List<FavouriteBasketProduct> Products { get; set; }
    }
    public class FavouriteBasketProduct
    {
        public Guid Id { get; set; }
        public int Sku { get; set; }
        public int Quantity { get; set; }
        public ProductType ProductType { get; set; }
        public List<int> SelectedIngredients { get; set; }
        public List<int> AddedIngredients { get; set; }
        public List<int> RemovedIngredients { get; set; }
        public List<FavouriteBasketProduct> Items { get; set; }
    }
    public class OrderInfo
    {
        public Guid Id { get; set; }
        public DateTimeOffset DateCreated { get; set; }
    }
    public enum ProductType
    {
        Pizza,
        Side,
        Dessert,
        Drink,
        Wrap
    }
}
