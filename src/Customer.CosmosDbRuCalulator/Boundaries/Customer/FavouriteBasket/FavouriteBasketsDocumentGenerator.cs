﻿using AutoFixture;
using AzureCosmosDB.Calculator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.FavouriteBasket
{
    public interface IFavouriteBasketsDocumentGenerator
        : IDocumentCollectionGenerator<FavouriteBasketsDocument, FavouriteBasketsDocumentGeneratorOptions>
    { }

    public class FavouriteBasketsDocumentGenerator : IFavouriteBasketsDocumentGenerator
    {
        private readonly IFixture _fixture;

        public FavouriteBasketsDocumentGenerator(IFixture fixture)
        {
            _fixture = fixture ?? throw new ArgumentNullException(nameof(fixture));
        }

        public Task<IEnumerable<FavouriteBasketsDocument>> GenerateAsync(FavouriteBasketsDocumentGeneratorOptions options)
        {
            var ids = Helpers.GenerateUniqueGuids(options.DocumentCount);

            var random = new Random();

            var items = _fixture.Build<FavouriteBasketsDocument>()
                .With(it => it.CustomerId, () => { var id = ids.First(); ids.RemoveAt(0); return id; })
                .With(it => it.Items, () => CreateBasket(random, options.ProductCountPerBasket, options.FavouriteBasketCountPerCustomer))
                .CreateMany(options.DocumentCount)
                .ToList();

            return Task.FromResult(items.AsEnumerable());
        }

        private List<FavouriteBasket> CreateBasket(Random random, int productCount, int basketCOunt)
        {
            return _fixture.Build<FavouriteBasket>()
                .With(it => it.Products, () => CreateProducts(random, productCount))
                .CreateMany(basketCOunt)
                .ToList();
        }
        private List<FavouriteBasketProduct> CreateProducts(Random random, int productCount)
        {
            return _fixture.Build<FavouriteBasketProduct>()
                .Without(it => it.Items)
                .With(it => it.RemovedIngredients, () => _fixture.CreateMany<int>(random.Next(2, 10)).ToList())
                .With(it => it.SelectedIngredients, () => _fixture.CreateMany<int>(random.Next(2, 10)).ToList())
                .With(it => it.AddedIngredients, () => _fixture.CreateMany<int>(random.Next(2, 10)).ToList())
                .CreateMany(productCount)
                .ToList();
        }
    }
}
