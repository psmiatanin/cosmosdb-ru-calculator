﻿using AzureCosmosDB.Calculator.Core;
using AzureCosmosDB.Calculator.Core.Configuration;
using Microsoft.Azure.Documents;
using System;
using System.Threading.Tasks;

namespace AzureCosmosDB.Calculator.Boundaries.Customer.FavouriteBasket
{
    public class FavouriteBasketsCRUDExecutor 
        : CRUDExecutor<FavouriteBasketsDocument, IFavouriteBasketsDocumentGenerator, FavouriteBasketsDocumentGeneratorOptions>
    {
        public FavouriteBasketsCRUDExecutor(ICRUDExecuteEngine engine, IFavouriteBasketsDocumentGenerator generator) 
            : base(engine, generator)
        { }

        protected override string Name => "Customer Favourite Baskets";

        public override Task ExecuteAsync(Func<Task<IDocumentClient>> clientFactory, string databaseId)
        {
            var collectionConfiguration = new CollectionConfiguration()
            {
                Id = "favourite-baskets",
                DatabaseId = databaseId,
                Recreate = true,
                PartitionKeys = new string[] { $"/{Helpers.GetJsonPropertyName<FavouriteBasketsDocument>(it => it.CustomerId)}" },
                IndexingPolicy = new IndexingPolicyConfiguration()
                {
                    Automatic = false,
                    Mode = IndexingMode.None
                }
            };

            var generatorOptions = new FavouriteBasketsDocumentGeneratorOptions()
            {
                DocumentCount = 10,
                FavouriteBasketCountPerCustomer = 5,
                ProductCountPerBasket = 5
            };

            return ExecuteCRUDAsync
            (
                clientFactory: clientFactory,
                collectionConfiguration: collectionConfiguration,
                getDocumentId: it => it.CustomerId.ToString(),
                getPartitionKey: it => it.CustomerId.ToString(),
                generatorOptions: generatorOptions,
                operations: CRUDOperations.All
            );
        }
    }
}
