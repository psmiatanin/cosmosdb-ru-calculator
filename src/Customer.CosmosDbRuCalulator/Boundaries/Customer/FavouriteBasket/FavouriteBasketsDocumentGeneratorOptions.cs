﻿
namespace AzureCosmosDB.Calculator.Boundaries.Customer.FavouriteBasket
{
    public class FavouriteBasketsDocumentGeneratorOptions
    {
        public int DocumentCount { get; set; }
        public int FavouriteBasketCountPerCustomer { get; set; }
        public int ProductCountPerBasket { get; set; }
    }
}
