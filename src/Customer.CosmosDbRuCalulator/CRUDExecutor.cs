﻿using AzureCosmosDB.Calculator.Core;
using Microsoft.Azure.Documents;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using AzureCosmosDB.Calculator.Core.Configuration;

namespace AzureCosmosDB.Calculator
{
    public abstract class CRUDExecutor<TDocument, TGenerator, TGeneratorOptions>
        where TGenerator : IDocumentCollectionGenerator<TDocument, TGeneratorOptions>
    {
        protected ICRUDExecuteEngine Engine { get; }
        protected TGenerator Generator { get; }

        protected CRUDExecutor(ICRUDExecuteEngine engine, TGenerator generator)
        {
            Engine = engine;
            Generator = generator;
        }

        protected abstract string Name { get; }

        public abstract Task ExecuteAsync(Func<Task<IDocumentClient>> clientFactory, string databaseId);

        protected virtual async Task ExecuteCRUDAsync(
                Func<Task<IDocumentClient>> clientFactory,
                CollectionConfiguration collectionConfiguration,
                Func<TDocument, string> getDocumentId,
                Func<TDocument, object> getPartitionKey,
                TGeneratorOptions generatorOptions,
                CRUDOperations operations = CRUDOperations.All)
        {
            Func<Task<IEnumerable<TDocument>>> generateDocuments =
                () => Task.Run(() => Generator.GenerateAsync(generatorOptions));

            var contexts =
                await Engine.ExecuteAsync
                (
                    clientFactory: clientFactory ?? throw new ArgumentNullException(nameof(clientFactory)),
                    collectionConfig: collectionConfiguration ?? throw new ArgumentNullException(nameof(collectionConfiguration)),
                    generateDocuments: generateDocuments ?? throw new ArgumentNullException(nameof(generateDocuments)),
                    getDocumentId: getDocumentId ?? throw new ArgumentNullException(nameof(getDocumentId)),
                    getDocumentPartitionKey: getPartitionKey ?? throw new ArgumentNullException(nameof(getPartitionKey)),
                    operations: operations
                );

            PrintResults(contexts);
        }

        protected virtual void PrintResults(IEnumerable<DocumentContext<TDocument>> items)
        {
            var builder = new StringBuilder();
            builder.AppendLine($"***** {Name}");
            builder.AppendLine($"Document Size: {Math.Round((decimal)items.Max(it => it.DocumentSizeInBytes / 1024.0), 2)}KB");
            builder.AppendLine($"Create Document: {items.Max(it => it.CreateRequestChargeRU ?? 0)}RU");
            builder.AppendLine($"Read Document: {items.Max(it => it.ReadRequestChargeRU ?? 0)}RU");
            builder.AppendLine($"Update Document: {items.Max(it => it.UpdateRequestChargeRU ?? 0)}RU");
            builder.AppendLine($"Delete Document: {items.Max(it => it.DeleteRequestChargeRU ?? 0)}RU");
            Console.WriteLine(builder.ToString());
        }
    }
}
