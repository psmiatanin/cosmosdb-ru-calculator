﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace AzureCosmosDB.Calculator
{
    public static class Helpers
    {
        public static Guid GenerateNotEmptyGuid()
        {
            var output = Guid.NewGuid();

            while (true)
            {
                if (output != Guid.Empty)
                {
                    return output;
                }
                output = Guid.NewGuid();
            }
        }

        public static List<Guid> GenerateUniqueGuids(int count, bool emptyAllowed = false)
        {
            var map = new Dictionary<Guid, Guid>();

            var id = Guid.Empty;

            for (var cnt = 0; cnt < count; cnt++)
            {
                while (true)
                {
                    id = emptyAllowed ? Guid.NewGuid() : GenerateNotEmptyGuid();

                    if (!map.ContainsKey(id))
                    {
                        map[id] = id;
                        break;
                    }
                }
            }

            return map.Select(it => it.Key).ToList();
        }

        public static string GetJsonPropertyName<T>(this Expression<Func<T, object>> expression)
        {
            var body = expression.Body as MemberExpression;

            if (body == null)
            {
                body = ((UnaryExpression)expression.Body).Operand as MemberExpression;
            }

            var attribute = body.Member.GetCustomAttribute<JsonPropertyAttribute>();

            return attribute == null ? body.Member.Name : attribute.PropertyName;
        }

    }
}
