# Introduction

This is the central repository for utility project to calculate [RU](https://docs.microsoft.com/en-us/azure/cosmos-db/request-units) for [Azure CosmsosDB](https://docs.microsoft.com/en-us/azure/cosmos-db/)

The project is aimed to help to optimize CosmosDB cost and define more or less accurate CosmosDB consumption plan. The utility can be used along with [Azure Capacity Calculator](https://cosmos.azure.com/capacitycalculator/) (sign in to use extended features) for analysis.

In general [Azure Capacity Calculator](https://cosmos.azure.com/capacitycalculator/) provides different (more frequently less) than real RU numbers retrieved programmically. Also the utility allows detecting RU for queries and each CRUD operation separately. [Azure Capacity Calculator](https://cosmos.azure.com/capacitycalculator/) - does not. For that reason the utility makes sense.

Features of the utulity:

1. Provides approach to create new database(s)/collection(s) programmicaly for an existing account, generate and seed data to them without any efforts and automatically. Also it allows working with already existing infrastructure and data. It allows create and test infrastructure and models which are very close to real ones. For data generation is was proposed to use [AutoFixture](https://github.com/AutoFixture/AutoFixture) library which allows code generation with minimum of code.
1. Allows RU calculation for every CRUD operation or some of them. Knowing RU for each CRUD operation separately is very usefull because if you aware of application logic and know what operation is more freaquent you can get more accurate total RU/s value. Mostly all operations can be used in case of new creating infrastructure and data generating. But if infrastructure exists and it already has data then it is possible to use only some specific operations (for example update/read without creating and deleting).
1. Allows RU calculation for queries.

NOTE: For now the application does not have unit tests and has not been tested properly. It has been implemented as quick solution to help me to create consumption plan and I did not have intension to implement real generic utility tool. But it can be improved and extended.

# Getting Started

## How to: Work With Temporary Test Infrastructure For CRUD

1. VS 2017+ or [VSCode](https://code.visualstudio.com/) shouls be used to work with the code
1. Create [Azure CosmosDB account](https://docs.microsoft.com/en-us/azure/cosmos-db/create-cosmosdb-resources-portal) in [Azure portal](https://portal.azure.com/).
1. Once CosmosDB account created, copy [account uri and primary key (or authentication token)](https://docs.microsoft.com/en-us/azure/cosmos-db/secure-access-to-data) to *appsettings.json* fine of the console entry point project.
1. Create data model to be generated and stored in a collection. Depending model complexity it is one or set of DTO classes with get/set properties
1. Implement data model generator. Data generator is a type which implements *IDocumentCollectionGenerator* interface
1. Implement CRUD executor for your model and collection. NOTE: Target database(s) and/or collection(s) can be created either automatically from code or manually. If the resources are created manually and you want manage them manually as well please ensure that in the code you have set flags *Recreate* and *ThrowExceptionIfExists* to false in configurations of database/collection

There are already implemented examples for customer address book, favourite baskets and so on. New stuff can be created based on those examples using mostly copy/past actions. Generally if you aware of the project details it will approximately take 15-30 minutes to create new set of classes for new model/collection.

## How to: Work With Queries

*`TODO: Will be updated a bit later`*

